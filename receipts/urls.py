from django.urls import path
from receipts.views import (
    show_expense_categories,
    show_receipts,
    create_receipt,
    show_accounts,
    create_expense_category,
    create_account,
)


urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expense_categories, name="show_categories"),
    path("categories/create/", create_expense_category, name="create_expense"),
    path("accounts/", show_accounts, name="show_accounts"),
    path("accounts/create/", create_account, name="create_account"),
]
