from django.shortcuts import render, redirect
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import AccountForm, ExpenseCategoryForm, ReceiptForm


@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required
def show_expense_categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/categories/list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect("show_categories")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}

    return render(request, "receipts/categories/create.html", context)


@login_required
def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts/list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("home")
    else:
        form = AccountForm()
    context = {"form": form}

    return render(request, "receipts/accounts/create.html", context)
