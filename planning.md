* [x] Fork Clone django two shot
* [x] cd into django two shot
* [x] code .
* [x] open VSC
* [x] create virtual 
* [x] upgrade pip
* [x] install django
* [x] install black
* [x] install flake8
* [x] install djhtml
* [x] install djlint
* [x] deactivate 
* [x] activate
* [x] pip freeze requirements.txt
* [x] django-admin startproject expenses .
* [x] makemigrations
* [x] createsuperuser
* [x] python manage.py test tests.test_feature_01
* [x] git add / commit / push
  *** Complete Feature 1 [5-tests-no-issues] ***
* [x] Create 2 apps in INSTALLED APPS
  * [x] django-admin startapp accounts 
  * [x] djang-admin startapp receipts
* [x] makemigrations
* [x] createsuperuser
* [x] test feature 2
  * [x] python manage.py test tests.test_feature_02
* [x] git add / commit / push
  *** Complete Feature 2 [5-tests-no-issues] ***
* [x] Create 3 Models in receipts app
        * [x] {had to use settings.AUTH_USER_MODEL}
  * [x] ExpenseCategory
    * [x] name propoerty / max length 50
    * [x] owner property / ForeignKey / USER / related "catagories" / CASCADE
    * [x] __str__ name
  * [x] Receipts
    * [x] vendor property / maxchar 200
    * [x] total property / decimalfield / 3 digits / 10 places
    * [x] tax property / DecimalField / 3 decimal places / max 10 digits
    * [x] date property / DateTimeField
    * [x] purchaser property / ForeignKey / User model / related "receipts" / CASCADE
    * [x] category propoerty / ForeignKey / ExpenseCategory model / related "receipts" / CASCADE
    * [x] account property / ForeignKey / Account model / related "receipts" / CASCADE / null=True
  * [x] Account 
    * [x] name property / max length 100
    * [x] number property / characters / max length 20
    * [x] owner property / ForeignKey / User model / related "accounts" / CASCADE
    * [x] __str__ name
* [x] python manage.py test tests.test_feature_03
      * [*] [ERROR] User not defined
      * [*] [RESEARCHED] settings.AUTH_USER_MODEL = User model
* [x] git add / commit / push
  *** Complete Feature 3 [28-tests-no-issues] *** 
* [x] register models in admin
* [x] python manage.py test tests.test_feature_04
* [x] makemigrations
* [x] git add / commit / push 
  *** Complete Feature 4 [3-tests-no-issues] ***
* [x] create show_receipt view for Receipt
  * [x] include template context
  * [x] register view in receipts app
    * [x] path "" name "home" new file named {receipts/urls.py}
  * [x] include URL for receipts app in expenses project
    * [x] import include
    * [x] "receipts/" include(".")
  * [x] `Create template for list view`
  * [x] `HTML:5`
    * [x] ` <main> tag`
      * [x] `<h1> "My Receipts"`
        * [x] `<table> -6 columns-`
          * [x] `<thead> <tr> <th> Vendor Total Tax Date Category Account`
          * [x] `<tbody>`
          * [x] `{%for%}`
          * [x] `<tr>`
          * [x] `<td> {{ receipt.vendor }}`
          * [x] `<td> {{ receipt.total }}`
          * [x] `<td> {{ receipt.tax }} `
          * [x] `<td> {{ receipt.date |dat:"m-d-Y" }} [RETURN-IN-M/D/Y]`
          * [x] `<td> {{ receipt.category }}`
          * [x] `<td> {{ receipt.account }}`
  * [x] test feature
  * [x] git add / commit / push
    *** Complete Feature 5 [6-tests-no-issues] ***
  * [x] in expenses.urls 
    * [x] use RedirectView "" -> path for list view 
    * [x] register that path to name home
      * [x] import from django.views.generic.base import RedirectView
        * [x] path "" -> RedirectView.as_view(url=reverse_lazy("home"))
    * [x] test feature 6
    * [x] git add / commit / push 
   *** Complete Feature 6 [2-tests-no-issues] *** 
  * [x] Register LoginView in accounts urls
     * [x] path 'login/' name 'login/'
  * [x] include url from accounts in expenses project 
    * [x] prefix "accounts/"
  * [x] create template in accounts
    * [x] create registration in templates
  * [x] Create HTML login.html in registration
    * [x] method=="post" in login.html + other
  * [x] in expense settings.py 
    * [x] create / set variable LOGIN_REDIRECT_URL = "home"
      * [x] redirect to path home (not made yet) 
  * [x] test feature 7 
  * [x] git add / commit / push 
   *** Complete Feature 7 [10-tests-no-issues] ***
  * [x] Change Receipt view to login required
    * [x] in views -> from django.contrib.auth.decorators import login_required
    * [x] over view include @login_required
  * [x] change queryset of view to filter receipt objects 
    * [x] purchaser=request.user
  * [x] test feature 8
  * [x] git add / commit / push 
   *** Complete Feature 8 [1-test-no-issues] ***
  * [x] import LogoutView in accounts urls
  * [x] register path "logout/" name "logout"
  * [x] in expense settings.py
    * [x] create / set variable LOGOUT_REDIRECT_URL = "login"
  * [x] test feature 9
  * [x] git add / commit / push 
   *** Complete Feature 9 [4-tests-no-issues] ***
  * [x] import UserCreationForm 
    * [x] from django.contrib.auth.forms import UserCreationForm
  * [x] create_user method -> create new user account from username + password
  * [x] use login function to log an account in
  * [x] after creating user 
    * [x] redirect browser to path registered with the name "home"
  * [x] create HTML signup.html in registration
    * [x] signup/ signup name="signup"
  * [x] use post form in signup.html 
  * [x] test feature 10
  * [x] git add / commit / push
   *** Complete Feature 10 [10-tests-no-issues] ***
 * [x] create a create view for Receipt model
   *  [x] from receipts.form import ReceiptForm (not made yet)
   * [x] @login_req
   * [x] if request.method == POST
     * [x] form = ReceiptForm(request.POST)
     * [x] validate form. save form
     * [x] redirect home
    * [x] else: form = Receiptform
    * [x] give context
    * [x] render receipts/create.html
  * [x] create forms.py in receipts
  * [x] from django import forms
  * [x] class -- forms.ModelForm
  * [x] class meta:
  * [x] model = Receipt 
   * [x] show vender 
   * [x] show total
   * [x] show tax
   * [x] show category
   * [x] show account
     * [x] [all in form]
     * [x] handle form submission to create new Receipt
  * [x] Must be logged in to see the view
  * [x] Register view in receipts.urls
    * [x] path "create/" -> name create_receipt
  * [x] Create HTML template that shows form to create a new receipt 
    * [x] Requirements
    * [x] `html:5`
    * [x] `<main>`
      * [x] `<div>`
        * [x] `<h1>Create Receipt</h1>`
        * [x] `<form method="post"></form>`
          * [x] form material [vender-total-tax-date-category-account-]
          * [x] create [button]
  * [x] python manage.py test tests.test_feature_11
  * [x] makemigrations
  * [x] git add / commit / push 
  *** Complete Feature 11 [13-tests-no-issues] ***
  * [ ] 